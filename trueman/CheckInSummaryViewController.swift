//
//  CheckInSummaryViewController.swift
//  truman
//
//  Created by Mashesh Somineni on 9/22/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import UIKit

class CheckInSummaryViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func confirmButtonClick(_ sender: Any) {
        
        self.present(title: "Truman", message: "Check In is completed. Thank you.", animated: true, dismiss: "Ok",onDismiss: {
            TabBarController.changeTo(tab: .Events)
        
        })

        self.present(message: "Your check in is completed. Thank you") {
            TabBarController.changeTo(tab: .Events)
        }
    }
   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
