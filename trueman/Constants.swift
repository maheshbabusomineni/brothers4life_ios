//
//  Constants.swift
//  trueman
//
//  Created by Mashesh Somineni on 9/18/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import UIKit

struct Constants {
    static let iBeaconCategoryIdentifier = "iBeaconCategoryIdentifier"
    static let iBeaconImmediateProximityActionIdentifier = "iBeaconImmediateProximityActionIdentifier"
    static let iBeaconExitActionIdentifier = "iBeaconExitActionIdentifier"
    static let appColor = "#D52E33"
    static let dateFormat = "dd-MMM-yyyy"
    static let fullDateFormat = "yyyy-MM-dd hh:mm:ss"
    static let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    static let appName = Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String
    enum CoreDataEntities : NSString{
        case Appointment = "Appointment"
        
    }
}
