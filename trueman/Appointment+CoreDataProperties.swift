//
//  Appointment+CoreDataProperties.swift
//  
//
//  Created by Mashesh Somineni on 9/26/17.
//
//

import Foundation
import CoreData


extension Appointment {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Appointment> {
        return NSFetchRequest<Appointment>(entityName: "Appointment")
    }

    @NSManaged public var shopid: String?
    @NSManaged public var timeslot: String?
    @NSManaged public var name: String?
    @NSManaged public var eventid: String?
    @NSManaged public var address: String?
    @NSManaged public var phone: String?
    @NSManaged public var email: String?
    @NSManaged public var gpslat: Double
    @NSManaged public var gpslong: Double
    @NSManaged public var receptionist: String?
    @NSManaged public var receptionistphoto: String?
    @NSManaged public var starttime: Int32
    @NSManaged public var endtime: Int32
    @NSManaged public var date: String?
    @NSManaged public var uuid: String?
    @NSManaged public var eventtype: Int32
    @NSManaged public var major: Int32
    @NSManaged public var minor: Int32

}

