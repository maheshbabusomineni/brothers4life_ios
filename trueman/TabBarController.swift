//
//  TabBarController.swift
//  truman
//
//  Created by Mashesh Somineni on 10/2/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import UIKit

enum TabBarItemsEnum:Int{
    case Events = 0
    case Appointments
    case CheckIns
}

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        for nav in self.viewControllers!  {
            (nav as! UINavigationController).popToRootViewController(animated: false)
        }
    }
}
extension TabBarController{
    class func changeTo(tab:TabBarItemsEnum?){
        if let newTab = tab {
            let tabbarController = Constants.appDelegate.window?.rootViewController as? UITabBarController
            tabbarController?.selectedIndex = newTab.rawValue
            let rootView = tabbarController?.viewControllers![newTab.rawValue] as! UINavigationController
            rootView.popToRootViewController(animated: false)
        }else{
            print("None selected")
        }
    }
    
}
