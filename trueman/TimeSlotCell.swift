//
//  TimeSlotCell.swift
//  truman
//
//  Created by Mashesh Somineni on 10/6/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import UIKit

class TimeSlotCell: UITableViewCell {
    @IBOutlet weak var slotLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var statusLable: UILabel!
    
    var item: Timeslot? {
        didSet {
            self.slotLabel?.text = item?.slot
            self.dateLabel?.text =  (item?.date) ?? "n/a"
            self.statusLable.text = (item?.booked ?? false) ? "Booked" : "Available"
            self.selectionStyle = (item?.booked ?? false) ? .none : .default
            self.statusLable.textColor = (item?.booked ?? false) ? UIColor.red : UIColor.green

        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
