//
//  RestAPIRouter.swift
//  trueman
//
//  Created by Mashesh Somineni on 9/13/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import Foundation
import Alamofire

public enum RestAPIRouter: URLRequestConvertible {
    case getAlerts()
    case registerDevice([String:Any])
    case getShops()
    case getComplaints()
    case getEvents()
    case getEventInfo([String:Any])

    var method: HTTPMethod {
        switch self {
        case .getAlerts:    fallthrough
        case .getComplaints:    fallthrough
        case .getShops():   fallthrough
        case .getEvents():  fallthrough
        case .getEventInfo(_):  return .get
        case .registerDevice(_):    return .post
        }
    }
    var path: String {
        switch self {
        case .getAlerts():
            return "/companies"
        case .registerDevice(_):
            return "/devices"
        case .getShops():
            return "/shops"
        case .getEvents():
            return "/events"
        case .getComplaints():
            return "/complaints"
        case .getEventInfo(_):
            return "/eventFullInfo"
        }
    }
    public func asURLRequest() throws -> URLRequest {
        let url = RestAPIController.sharedInstance.baseUrl.appendingPathComponent(self.path)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        let emptyParams: [String:Any] = [:]
        
        switch self {
        case .getAlerts():  fallthrough
        case .getEvents():  fallthrough
        case .getComplaints(): fallthrough
        case .getShops():
            urlRequest = try URLEncoding.queryString.encode(urlRequest, with: emptyParams)
        case .getEventInfo(let bodyParams):
            urlRequest = try URLEncoding.queryString.encode(urlRequest, with: bodyParams)
        case .registerDevice(let bodyParams):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with:bodyParams)
       
        }
        return urlRequest
            
    }
}
