//
//  ComplaintListCell.swift
//  truman
//
//  Created by Mashesh Somineni on 9/28/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import UIKit

class ComplaintListCell: UITableViewCell {
    
    var item: Complaint? = nil {
        didSet {
            self.textLabel?.text = item?.complaint
            self.detailTextLabel?.text = item?.recommendationmessage
            
            /*if let item = item {
             imgIcon.image = Icons(rawValue: item.icon)?.image()
             lblName.text = item.name
             lblLocation.text = item.locationString().0
             } else {
             imgIcon.image = nil
             lblName.text = ""
             lblLocation.text = ""
             }*/
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
