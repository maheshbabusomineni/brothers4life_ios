//
//  InstitutesListController.swift
//  truman
//
//  Created by Mashesh Somineni on 10/8/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import UIKit

class InstitutesListController:CommonController{
    static let sharedInstance = InstitutesListController()
    var delegate:CommonDelegate?
    var objectsArray: [Institute] = [Institute]()
    override init() {
    }
    func processServiceRequest(request:RestAPIRouter) {
        RestAPIController.startRequest(request:request){ serverResponse in
            print("Response = \(serverResponse)")
            guard serverResponse.error == nil else {
                self.delegate?.showAlert(message: (serverResponse.error?.localizedDescription)!)
                return
            }
            guard let responseArray = serverResponse.json?.array else { return }
            
            self.objectsArray.removeAll()
            for dictionary in responseArray{
                let object = Institute(dictionary: dictionary)
                self.objectsArray.append(object)
            }
            self.delegate?.listRefresh()
        }
    }
}

