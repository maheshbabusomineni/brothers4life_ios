//
//  ViewController.swift
//  trueman
//
//  Created by Mashesh Somineni on 9/12/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!

    var companiesArray:[Company] = [Company]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Make a service call to get the data from server
        RestAPIController.startRequest(request: RestAPIRouter.getAlerts()){ serverResponse in
            
            //print("Response = \(serverResponse)")
            guard let responseArray = serverResponse.json?.array else { return }
            
            for dictionary in responseArray{
                let company = Company(dictonary: dictionary)
                self.companiesArray.append(company)
            }
            
            //Update UI on Main Thread
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ViewController:UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return companiesArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "Cell") as UITableViewCell!
        
        // set the text from the data model
        cell.textLabel?.text = self.companiesArray[indexPath.row].name
        cell.detailTextLabel?.text = self.companiesArray[indexPath.row].address
        
        return cell

    }
    
}

