//
//  AnnotationViewExtension.swift
//  truman
//
//  Created by Mashesh Somineni on 9/25/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import UIKit
import MapKit
extension MKAnnotationView {
    
    func loadCustomAnnotation(){
        
        let stackView2 = UIStackView()
        stackView2.axis = .horizontal
        stackView2.alignment = .lastBaseline
        stackView2.distribution = .fill
        stackView2.spacing = 8
        
        let button:UIButton = UIButton(type: .system)
        button.setTitle("Button1", for: .normal)
        //button.addTarget(self, action: #selector(self.buttonTapped(button:)) , for: .touchUpInside)
        
        let button2:UIButton = UIButton(type: .system)
        button2.setTitle("Button2", for: .normal)
        //button2.addTarget(self, action: #selector(self.buttonTapped(button:)) , for: .touchUpInside)
        
        stackView2.addArrangedSubview(button as UIView)
        stackView2.addArrangedSubview(button2 as UIView)
        
        
        self.detailCalloutAccessoryView = stackView2
        
    }
}
