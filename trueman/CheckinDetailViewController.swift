//
//  CheckinDetailViewController.swift
//  truman
//
//  Created by Mashesh Somineni on 9/28/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import UIKit
import SwiftyJSON

class CheckinDetailViewController: UIViewController {
    @IBOutlet weak var driverNameLabel: UILabel!
    @IBOutlet weak var recomandationLabel: UILabel!
    @IBOutlet weak var shopAddressLabel: UILabel!
    @IBOutlet weak var shopNameLabel: UILabel!
    @IBOutlet weak var complaintLabel: UILabel!
    @IBOutlet weak var vehicleNameLable: UILabel!
    
    var appointment:Appointment? {
        didSet{
            self.shopNameLabel.text = appointment?.name
            self.shopAddressLabel.text = appointment?.address
            self.shopAddressLabel.numberOfLines = 0
            self.shopAddressLabel.sizeToFit()
        }
    }
    
    var eventInfoDictionary:JSON? {
        didSet {
            //FIXME:Implement model object for this
            if let driver = eventInfoDictionary?["driver"] {
                driverNameLabel.text = "\(driver["firstname"].string ?? "First Name") \(driver["lastname"].string ?? "Last Name")"
            }
            if let vechicle = eventInfoDictionary?["vehicle"] {
                vehicleNameLable.text = "\(vechicle["make"].string ?? "") "
            }
            if let recommandation = eventInfoDictionary?["recomandation"] {
                if let title = recommandation["event"].string ?? recommandation["complaint"].string {
                    complaintLabel.text = title
                }
                recomandationLabel.text = "\(recommandation["recommendation"].string ?? "Shop Repair") "
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        guard let selectedAppointmentId = GlobalSingleton.sharedInstance.selectedAppointmentId else { return }
        self.appointment = Appointment.fetchByEventId(eventId: selectedAppointmentId, context: Constants.managedObjectContext)!
        
        let params:[String:Any] = ["eventid":Int(self.appointment!.eventid!) ?? "n/a","eventtype":Int((self.appointment?.eventtype)!) ]
        
        print(params)
        RestAPIController.startRequest(request: RestAPIRouter.getEventInfo(params)){ serverResponse in
            
            guard serverResponse.error == nil else {
                self.present(message: (serverResponse.error?.localizedDescription)!)
                return
            }
            guard let responseData = serverResponse.json else { return }
    
             //Update UI on Main Thread
             DispatchQueue.main.async {
                self.eventInfoDictionary = responseData
             }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
