//
//  PlacemarkExtension.swift
//  truman
//
//  Created by Mashesh Somineni on 10/5/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import CoreLocation
extension CLPlacemark {
    
    var compactAddress: String? {
        if let name = name {
            var result = name
            
            if let street = thoroughfare {
                result += ", \(street)"
            }
            
            if let city = locality {
                result += ", \(city)"
            }
            
            if let country = country {
                result += ", \(country)"
            }
            
            return result
        }
        
        return nil
    }
    
}
