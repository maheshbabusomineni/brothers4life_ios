//
//  iBeaconsListViewController.swift
//  trueman
//
//  Created by Mashesh Somineni on 9/18/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import UIKit

import UIKit
import CoreLocation
import UserNotifications

let storedItemsKey = "storedItems"

class iBeaconsListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var items = [iBeacon]()
    let locationManager = CLLocationManager()
    var alertShowin = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        
        loadItems()
    }
    
    func loadItems() {
        guard let storedItems = UserDefaults.standard.array(forKey: storedItemsKey) as? [Data] else { return }
        for itemData in storedItems {
            guard let item = NSKeyedUnarchiver.unarchiveObject(with: itemData) as? iBeacon else { continue }
            items.append(item)
            startMonitoringItem(item)
        }
    }
    
    func persistItems() {
        var itemsData = [Data]()
        for item in items {
            let itemData = NSKeyedArchiver.archivedData(withRootObject: item)
            itemsData.append(itemData)
        }
        UserDefaults.standard.set(itemsData, forKey: storedItemsKey)
        UserDefaults.standard.synchronize()
    }
    
    func startMonitoringItem(_ item: iBeacon) {
        let beaconRegion = item.asBeaconRegion()
        locationManager.startMonitoring(for: beaconRegion)
        locationManager.startRangingBeacons(in: beaconRegion)
    }
    
    func stopMonitoringItem(_ item: iBeacon) {
        let beaconRegion = item.asBeaconRegion()
        locationManager.stopMonitoring(for: beaconRegion)
        locationManager.stopRangingBeacons(in: beaconRegion)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueAdd", let viewController = segue.destination as? AddiBeaconViewController {
            viewController.delegate = self
        }
    }
}

extension iBeaconsListViewController:AddBeacon {
    func addBeacon(item: iBeacon) {
        items.append(item)
        
        tableView.beginUpdates()
        let newIndexPath = IndexPath(row: items.count - 1, section: 0)
        tableView.insertRows(at: [newIndexPath], with: .automatic)
        tableView.endUpdates()
        
        startMonitoringItem(item)
        persistItems()
    }
}

// MARK: UITableViewDataSource
extension iBeaconsListViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "iBeaconCell", for: indexPath) as! iBeaconCell
        cell.item = items[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            stopMonitoringItem(items[indexPath.row])
            
            tableView.beginUpdates()
            items.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
            
            persistItems()
        }
    }
    
}

// MARK: UITableViewDelegate
extension iBeaconsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        self.performSegue(withIdentifier: "ParkingSlotToCheckInSummarySegue", sender: self)
    }
}

// MARK: CLLocationManagerDelegate
extension iBeaconsListViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("Failed monitoring region: \(error.localizedDescription)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location manager failed: \(error.localizedDescription)")
    }
    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        
        // Find the same beacons in the table.
        var indexPaths = [IndexPath]()
        for beacon in beacons {
            for row in 0..<items.count {
                if items[row] == beacon {
                    items[row].beacon = beacon
                    indexPaths += [IndexPath(row: row, section: 0)]
                }
            }
        }
        
        // Update beacon locations of visible rows.
        for indexPath in indexPaths {
            let cell = tableView.cellForRow(at: indexPath) as! iBeaconCell
            if(cell.refreshLocation()){
                if UIApplication.shared.applicationState == .background {
                    let content = UNMutableNotificationContent()
                    content.title = "Truman"
                    content.body = "Immediate proximity"
                    content.sound = UNNotificationSound.default()
                    
                    // Deliver the notification in five seconds.
                    let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 5, repeats: false)
                    let request = UNNotificationRequest.init(identifier:Constants.iBeaconCategoryIdentifier , content: content, trigger: trigger)
                    
                    // Schedule the notification.
                    let center = UNUserNotificationCenter.current()
                    center.removeAllDeliveredNotifications()
                    center.add(request) { (error) in
                        print(error?.localizedDescription ?? "")
                    }
                }else{
                    if !alertShowin {
                        self.alertShowin = true
                        let alertController = UIAlertController(title: nil, message: "Truck is parked in the selected slot.", preferredStyle: .alert)
                        
                        let OKAction = UIAlertAction(title: "OK", style: .default) { action in
                            // ...
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true) {
                            // ...
                        }
                    }
                    
                }
            }
            
        }
        
    }
}

