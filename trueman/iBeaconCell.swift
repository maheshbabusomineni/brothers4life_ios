//
//  iBeaconCell.swift
//  trueman
//
//  Created by Mashesh Somineni on 9/18/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import UIKit

class iBeaconCell: UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    
    var item: iBeacon? = nil {
        didSet {
            if let item = item {
                imgIcon.image = Icons(rawValue: item.icon)?.image()
                lblName.text = item.name
                lblLocation.text = item.locationString().0
            } else {
                imgIcon.image = nil
                lblName.text = ""
                lblLocation.text = ""
            }
        }
    }
    
    func refreshLocation() -> Bool{
        
        let proximityDetails = item?.locationString()
        lblLocation.text = proximityDetails?.0
        return (proximityDetails?.1)!
    }
}

