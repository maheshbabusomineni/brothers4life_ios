//
//  Company.swift
//  trueman
//
//  Created by Mashesh Somineni on 9/12/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import Foundation
import SwiftyJSON

class Company {
    var id:Int = 0
    var name:String = ""
    var age:Int = 0
    var address:String = ""
    var salary:Int = 0
    
    init(dictonary:JSON) {
        if let value = dictonary["id"].int  {
            self.id = value
        }
        if let value = dictonary["name"].string {
            self.name = value
        }
        if let value = dictonary["age"].int {
            self.age = value
        }
        if let value = dictonary["address"].string {
            self.address = value
        }
        if let value = dictonary["salary"].int {
            self.salary = value
        }
    }
    
}
