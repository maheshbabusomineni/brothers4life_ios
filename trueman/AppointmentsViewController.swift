//
//  AppointmentsViewController.swift
//  truman
//
//  Created by Mashesh Somineni on 9/27/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import UIKit
import FSCalendar
import CoreLocation
import MapKit

class AppointmentsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var appointmentsArray = [Appointment]()
    @IBOutlet weak var calendar: FSCalendar!
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy"
        return formatter
    }()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.estimatedRowHeight = 120
        
        self.calendar.select(Date())
        self.calendar.scope = .month
        self.calendar.select(Date().tomorrow, scrollToDate: true)
        
        // For UITest
        self.calendar.accessibilityIdentifier = "calendar"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.refresh(date: Date().tomorrow.toString(dateFormat: Constants.dateFormat))
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }    
    func refresh(date:String){
        self.appointmentsArray = Appointment.fetchByDate(date: date, context: Constants.managedObjectContext) as! [Appointment]
        
        self.tableView.reloadData()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "AppointmentToDirectionsSegue"{
            
            if let indexPath = self.tableView.indexPathForView(sender as! UIView) {
                let appointment = self.appointmentsArray[indexPath.row]
                let destination = segue.destination as! MapDirectionsViewController
                //destination.selectedAppointment = appointment
                GlobalSingleton.sharedInstance.selectedAppointmentId = appointment.eventid
            }
        }
    }

}

extension AppointmentsViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Appointments"
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.appointmentsArray.count

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AppointmentsListCell", for: indexPath) as! AppointmentsListCell
        cell.item = self.appointmentsArray[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let appointment = self.appointmentsArray[indexPath.row]
        GlobalSingleton.sharedInstance.selectedAppointmentId = appointment.eventid
    }
}
extension AppointmentsViewController:FSCalendarDataSource, FSCalendarDelegate, UIGestureRecognizerDelegate {
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let shouldBegin = self.tableView.contentOffset.y >= -self.tableView.contentInset.top
        return shouldBegin
    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.view.layoutIfNeeded()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.refresh(date: self.dateFormatter.string(from: date))
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        print("\(self.dateFormatter.string(from: calendar.currentPage))")
    }
    
    
}
