//
//  CommonController.swift
//  truman
//
//  Created by Mashesh Somineni on 10/8/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import UIKit

protocol EventsControllerDelegate {
    func refresh()
    func showAlert(message:String)
    func changeSegmentValue(segmentValue:Int)
}
protocol CommonDelegate {
    func showAlert(message:String)
    func listRefresh()
}

class CommonController {

}
