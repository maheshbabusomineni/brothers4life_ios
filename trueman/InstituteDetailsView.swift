//
//  InstituteDetailsView.swift
//  truman
//
//  Created by Mashesh Somineni on 10/8/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import UIKit

class InstituteDetailsView: UIStackView {

    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var nameLebel: UILabel!
    
    var item:Institute? {
        didSet{
            self.nameLebel.text = self.item?.name
            self.addressLabel.text = self.item?.address
            self.cityLabel.text = self.item?.city
            self.phoneLabel.text = self.item?.phone
        }
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
