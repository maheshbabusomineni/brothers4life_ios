//
//  GoogleMapViewController.swift
//  truman
//
//  Created by Mashesh Somineni on 9/29/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

enum TravelModes: Int {
    case driving
    case walking
    case bicycling
}
class GoogleMapViewController: UIViewController {

    @IBOutlet weak var viewMap: GMSMapView!
    @IBOutlet weak var lblInfo: UILabel!
    var locationManager = CLLocationManager()
    var didFindMyLocation = false
    
    //var mapTasks = MapTasks()
    
    var locationMarker: GMSMarker!
    var originMarker: GMSMarker!
    var destinationMarker: GMSMarker!
    var routePolyline: GMSPolyline!
    var markersArray: Array<GMSMarker> = []
    var waypointsArray: Array<String> = []
    var travelMode = TravelModes.driving
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
