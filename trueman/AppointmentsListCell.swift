//
//  AppointmentsListCell.swift
//  truman
//
//  Created by Mashesh Somineni on 9/22/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import UIKit

class AppointmentsListCell: UITableViewCell {
    
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var shopNameLabel: UILabel!

    var item: Appointment? = nil {
        didSet {
            self.titleLable.text = item?.name
            self.shopNameLabel.text = "\((item?.date)!) \((item?.timeslot)!) "
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
