//
//  Timeslot.swift
//  truman
//
//  Created by Mashesh Somineni on 9/26/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import Foundation
class Timeslot{
    
    var slot:String?
    var booked:Bool = false
    var shopid:String?
    var date:String
    
    init(shopid:String,slot:String) {
        self.slot = slot
        self.shopid = shopid
        self.date = (Date().tomorrow).toString(dateFormat:Constants.dateFormat)
    }
}
