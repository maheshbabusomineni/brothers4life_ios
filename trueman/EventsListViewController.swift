//
//  EventsListViewController.swift
//  truman
//
//  Created by Mashesh Somineni on 9/22/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import UIKit

class EventsListViewController: UITableViewController {
    var eventsController = EventsController.sharedInstance
    @IBOutlet weak var sementControl: UISegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.eventsController.delegate = self
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl?.addTarget(self, action: #selector(refreshControlValueChanged), for: .valueChanged)
    }
    
    
    func refreshControlValueChanged(_sender: Any) {
        self.eventsController.selectedEventType = EventType(rawValue: sementControl.selectedSegmentIndex)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.eventsController.processServiceRequest()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func segmentValueChanged(_ sender: Any) {
        eventsController.selectedEventType = EventType(rawValue: (sender as! UISegmentedControl).selectedSegmentIndex) ?? .vehicle
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return eventsController.eventsArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventsListCell", for: indexPath) as! EventsListCell
        cell.item = eventsController.eventsArray[indexPath.row]
        return cell

    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "EventListToShopsViewSegue", sender: self)
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "EventListToShopsViewSegue"{
            if let indexpath = self.tableView.indexPathForSelectedRow {
                guard let eventid = Int(eventsController.eventsArray[indexpath.row].eventid ?? "n/a") else {
                    return
                }
                eventsController.selectedEventId = eventid
            }
         
        }
        
    }
}
extension EventsListViewController:EventsControllerDelegate{
    func refresh() {
        //Update UI on Main Thread
        DispatchQueue.main.async {
            
            if self.refreshControl != nil {
                self.refreshControl?.endRefreshing()
            }
            self.tableView.reloadData()
        }
    }
    func showAlert(message: String) {
        DispatchQueue.main.async {
            if self.refreshControl != nil {
                self.refreshControl?.endRefreshing()
            }
            self.showAlert(message: message)
        }
    }
    func changeSegmentValue(segmentValue: Int) {
        if self.refreshControl != nil {
            self.refreshControl?.endRefreshing()
        }
        self.sementControl.selectedSegmentIndex = segmentValue
    }
}
