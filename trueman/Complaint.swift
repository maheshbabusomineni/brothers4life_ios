//
//  Event.swift
//  truman
//
//  Created by Mashesh Somineni on 9/22/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import Foundation
import SwiftyJSON

class Complaint {
    
    var complaintid:String?
    var complaint:String?
    var recommendationmessage:String?
    var severity:String?
    var action:String?
    var recommendation:String?
    var confidence:String?
    var complaintdate:String?
    var driverid:String?
    var vehicleid:String?
    var startdate:String?
    var deviceid:String?
    
    init(dictionary:JSON) {
        if let value = dictionary["complaintid"].string { self.complaintid  = value }
        if let value = dictionary["complaint"].string { self.complaint  = value }
        if let value = dictionary["recommendationmessage"].string { self.recommendationmessage  = value }
        if let value = dictionary["severity"].string { self.severity  = value }
        if let value = dictionary["action"].string { self.action  = value }
        if let value = dictionary["recommendation"].string { self.recommendation  = value }
        if let value = dictionary["confidence"].string { self.confidence  = value }
        //if let value = dictionary["complaintdate"].{ self.complaintdate  = value }
        if let value = dictionary["driverid"].string{ self.driverid  = value }
        if let value = dictionary["vehicleid"].string { self.vehicleid  = value }
        //if let value = dictionary["startdate"] as? Date{ self.startdate  = value }
        if let value = dictionary["deviceid"].string { self.deviceid  = value }
        
    }
    
    /*init(dictionary:[String:Any]){
     if let value = dictionary["eventid"] as? Int{ self.eventid  = value }
     if let value = dictionary["event"] as? String{ self.event  = value }
     if let value = dictionary["description"] as? String{ self.description  = value }
     if let value = dictionary["severity"] as? String{ self.severity  = value }
     if let value = dictionary["action"] as? String{ self.action  = value }
     if let value = dictionary["recommendation"] as? String{ self.recommendation  = value }
     if let value = dictionary["confidence"] as? String{ self.confidence  = value }
     if let value = dictionary["eventdate"] as? Date{ self.eventdate  = value }
     if let value = dictionary["driverid"] as? Int{ self.driverid  = value }
     if let value = dictionary["vehicleid"] as? Int{ self.vehicleid  = value }
     if let value = dictionary["startdate"] as? Date{ self.startdate  = value }
     if let value = dictionary["deviceid"] as? Int{ self.deviceid  = value }
     
     }*/
}
