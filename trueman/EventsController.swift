//
//  EventsController.swift
//  truman
//
//  Created by Mashesh Somineni on 10/2/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import Foundation

enum EventType:Int{
    case vehicle = 0
    case driver
}
class EventsController:CommonController {
    static let sharedInstance = EventsController()
    var eventsArray:[Event] = [Event]()
    var delegate:EventsControllerDelegate?
    var selectedEventId = 0
    var selectedEventType:EventType? {
        didSet{
            self.processServiceRequest()
        }
    }
    override init() {
        self.eventsArray = [Event]()
    }
    func processServiceRequest() {
        let request:RestAPIRouter
        switch self.selectedEventType ?? .vehicle{
        case .vehicle:
            request = RestAPIRouter.getEvents()
            break
        case .driver:
            request = RestAPIRouter.getComplaints()
            break
        }
        RestAPIController.startRequest(request:request){ serverResponse in
            
            print("Response = \(serverResponse)")
            guard serverResponse.error == nil else {
                self.delegate?.showAlert(message: (serverResponse.error?.localizedDescription)!)
                return
            }
            guard let responseArray = serverResponse.json?.array else { return }
            
            self.eventsArray.removeAll()
            for dictionary in responseArray{
                let object = Event(dictionary: dictionary)
                self.eventsArray.append(object)
            }
            self.delegate?.refresh()
        }
    }
    
    func addAnEvent(userInfo:[AnyHashable : Any]) {
        
        guard let table = userInfo["table"] as? String else {
            return
        }
        
        self.selectedEventType = (table == "complaint") ? .driver : .vehicle
        self.delegate?.changeSegmentValue(segmentValue: (self.selectedEventType?.rawValue) ?? 0)
    }
}

