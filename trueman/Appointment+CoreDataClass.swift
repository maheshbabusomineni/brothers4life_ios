//
//  Appointment+CoreDataClass.swift
//
//
//  Created by Mashesh Somineni on 9/26/17.
//
//

import Foundation
import CoreData
import Contacts
import MapKit

@objc(Appointment)
public class Appointment: NSManagedObject {
    // Insert code here to add functionality to your managed object subclass
    class func insertObject(dictionary:[String:Any], context:NSManagedObjectContext) {
        
        // Create Managed Object
        let entityDescription = NSEntityDescription.entity(forEntityName: Constants.CoreDataEntities.Appointment.rawValue as String, in: context)
        
        //Create new entity
        let newEntity:Appointment = NSManagedObject(entity: entityDescription!, insertInto: context) as! Appointment
        
        if let shop = dictionary["shop"] as? MaintenanceShop {
            if let value = shop.id { newEntity.shopid = value }
            if let value = shop.name { newEntity.name = value }
            if let value = shop.longitude { newEntity.gpslong = value }
            if let value = shop.latitude { newEntity.gpslat = value }
            if let value = shop.address { newEntity.address = value }
            if let value = shop.email { newEntity.email = value }
            if let value = shop.phone { newEntity.phone = value }
            if let value = shop.logo { newEntity.receptionistphoto = value }
            if let value = shop.open_time { newEntity.starttime = Int32(value) }
            if let value = shop.close_time { newEntity.endtime = Int32(value) }
        }
        
        if let timeslot = dictionary["timeslot"] as? Timeslot {
            newEntity.timeslot = timeslot.slot
            newEntity.date = timeslot.date
        }
        if let eventid = dictionary["eventid"] as? Int{
            
            print(eventid)
            newEntity.eventid = "\(eventid)"
        
        }
        if let eventtype = dictionary["eventtype"] as? Int{ newEntity.eventtype = Int32(eventtype)}
        
        //Save the object
        do {
            try newEntity.managedObjectContext?.save()
        } catch {
            print(error)
        }
    }
    class func fetchByDate(date:String, context:NSManagedObjectContext) -> NSArray {
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: Constants.CoreDataEntities.Appointment.rawValue as String, in: context)
        
        fetchRequest.predicate = NSPredicate(format: "date == %@", date)
        let sortDescriptor = NSSortDescriptor(key: "starttime", ascending: true)

        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        do {
            
            let result = try context.fetch(fetchRequest)
            return result as NSArray
            
        } catch {
            
            print(error as NSError)
            return NSArray()
        }
    }
    class func fetchByEventId(eventId:String, context:NSManagedObjectContext) -> Appointment? {
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: Constants.CoreDataEntities.Appointment.rawValue as String, in: context)
        
        fetchRequest.predicate = NSPredicate(format: "eventid == %@", eventId)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        
        do {
            
            let result = try context.fetch(fetchRequest)
            return result.last as? Appointment
            
        } catch {
            
            print(error as NSError)
            return nil
        }
    }
    class func updateObject() {
        
        
    }
    
    class func truncateAllObjects(context:NSManagedObjectContext) {
        
        CommonOperations.truncateAllObjects(entityName: Constants.CoreDataEntities.Appointment.rawValue as String, context: context)
    }
    class func fetchAllObjects(context:NSManagedObjectContext) -> NSArray {
        
        let resultArray:[Appointment] = CommonOperations.fetchAllObjects(entityName: Constants.CoreDataEntities.Appointment.rawValue as String, context: context) as! [Appointment]
        return resultArray as NSArray
    }
    class func getFirstAppointment(context:NSManagedObjectContext) -> Appointment {
        return Appointment.fetchAllObjects(context: context).firstObject as! Appointment
    }
    
    func mapItem() -> MKMapItem {
        let addressDict = [CNPostalAddressStreetKey: address!]
        let placemark = MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: self.gpslat, longitude: self.gpslong), addressDictionary: addressDict)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = self.name
        return mapItem
    }
    
}
