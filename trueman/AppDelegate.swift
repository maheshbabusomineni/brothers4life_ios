//
//  AppDelegate.swift
//  trueman
//
//  Created by Mashesh Somineni on 9/12/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    var locationManager = CLLocationManager()
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        Settings.registerDefaults()

        self.enablePushNotifications()
        
        
        
        //Navigation bar properties
        UINavigationBar.appearance().barTintColor = UIColor.hexStringToUIColor(hex: Constants.appColor)
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        
        // Check if launched from notification
        // 1
        if let notification = launchOptions?[.remoteNotification] as? [String: AnyObject] {
            // 2
            
            print(notification)
            //let aps = notification["aps"] as! [String: AnyObject]
            //_ = NewsItem.makeNewsItem(aps)
            // 3
            (window?.rootViewController as? UITabBarController)?.selectedIndex = 1
        }
        locationManager.delegate = self
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if (appDelegate.window?.rootViewController as? UITabBarController)?.selectedIndex == 0{
            EventsController.sharedInstance.processServiceRequest()
        }

    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "trueman")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

// MARK: Push Notifications
extension AppDelegate:UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        if ((notification.request.content.userInfo["action"] as! String) == "INSERT") {
            EventsController.sharedInstance.addAnEvent(userInfo:notification.request.content.userInfo)
            completionHandler([.alert, .badge, .sound])
        }else{
            EventsController.sharedInstance.processServiceRequest()
        }
        
   }
    func enablePushNotifications(){
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            guard granted else { return }
            let viewAction = UNNotificationAction(identifier: Constants.iBeaconImmediateProximityActionIdentifier,
                                                  title: "Do it again?",
                                                  options: [.foreground])
            
            let iBeaconCategory = UNNotificationCategory(identifier: Constants.iBeaconCategoryIdentifier,
                                                      actions: [viewAction],
                                                      intentIdentifiers: [],
                                                      options: [])
            
            UNUserNotificationCenter.current().setNotificationCategories([iBeaconCategory])
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                guard settings.authorizationStatus == .authorized else { return }
                
                DispatchQueue.main.async {
                    let isRegisteredForRemoteNotifications = UIApplication.shared.isRegisteredForRemoteNotifications
                    if !isRegisteredForRemoteNotifications {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
            }
        }
    }
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        
        //Make a service call to get the data from server
        RestAPIController.startRequest(request: RestAPIRouter.registerDevice(["accesstoken":token,"osversion":UIDevice.current.systemVersion,"devicemodel":UIDevice.current.localizedModel])){ serverResponse in
            guard let error  = serverResponse.error else {
                print("Device registered successfully")
                return
            }
            
            print("Error while registering device = \(error.localizedDescription)")
        }
        print("Device Token: \(token)")
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        //print("Failed to register: \(error)")
    }
    func application(
        _ application: UIApplication,
        didReceiveRemoteNotification userInfo: [AnyHashable : Any],
        fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print("Notification recieved")
        
        
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if response.actionIdentifier == Constants.iBeaconExitActionIdentifier {
            (window?.rootViewController as? UITabBarController)?.selectedIndex = 2
        }
        
    }
}

// MARK: CLLocationManagerDelegate
extension AppDelegate: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
       
    }
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        guard region is CLBeaconRegion else { return }
        let content = UNMutableNotificationContent()
        content.title = "Truman"
        content.body = "Entered shop location"
        content.sound = .default()
        content.categoryIdentifier = Constants.iBeaconCategoryIdentifier
        let request = UNNotificationRequest(identifier: Constants.iBeaconExitActionIdentifier, content: content, trigger: nil)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        let content = UNMutableNotificationContent()
        content.title = "Truman"
        content.body = "Truck is in the shop location"
        content.sound = .default()
        content.categoryIdentifier = Constants.iBeaconCategoryIdentifier
        let request = UNNotificationRequest(identifier: Constants.iBeaconExitActionIdentifier, content: content, trigger: nil)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }

}

