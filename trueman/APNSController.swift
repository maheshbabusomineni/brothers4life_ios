//
//  APNSController.swift
//  trueman
//
//  Created by Mashesh Somineni on 9/21/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import UIKit
import UserNotifications

class APNSController {
    public static let sharedInstance = APNSController()
    public init() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            guard granted else { return }
            let viewAction = UNNotificationAction(identifier: Constants.iBeaconImmediateProximityActionIdentifier,
                                                  title: "Do it again?",
                                                  options: [.foreground])
            
            let iBeaconCategory = UNNotificationCategory(identifier: Constants.iBeaconCategoryIdentifier,
                                                         actions: [viewAction],
                                                         intentIdentifiers: [],
                                                         options: [])
            
            UNUserNotificationCenter.current().setNotificationCategories([iBeaconCategory])
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                guard settings.authorizationStatus == .authorized else { return }
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }

}
extension APNSController {
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
   
    }
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        //print("Failed to register: \(error)")
    }
    func application(
        _ application: UIApplication,
        didReceiveRemoteNotification userInfo: [AnyHashable : Any],
        fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        
        
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if response.actionIdentifier == Constants.iBeaconExitActionIdentifier {
            print("App exiting button click")
        }
        
    }
}
