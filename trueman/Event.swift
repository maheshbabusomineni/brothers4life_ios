//
//  Event.swift
//  truman
//
//  Created by Mashesh Somineni on 9/22/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import Foundation
import SwiftyJSON

class Event {
    var eventid:String?
    var event:String?
    var description:String?
    var severity:String?
    var action:String?
    var recommendation:String?
    var confidence:String?
    var eventdate:String?
    var driverid:String?
    var vehicleid:String?
    var startdate:String?
    var deviceid:String?
    var eventType:Int?
    
    init(dictionary:JSON) {
        if let value = dictionary["eventid"].string ?? dictionary["complaintid"].string { self.eventid  = value }
        if let value = dictionary["event"].string ?? dictionary["complaint"].string { self.event  = value }
        if let value = dictionary["description"].string ?? dictionary["recommendationmessage"].string { self.description  = value }
        if let value = dictionary["severity"].string { self.severity  = value }
        if let value = dictionary["action"].string { self.action  = value }
        if let value = dictionary["recommendation"].string { self.recommendation  = value }
        if let value = dictionary["confidence"].string { self.confidence  = value }
        if let value = dictionary["eventdate"].string ?? dictionary["complaintdate"].string { self.eventdate  = value }
        if let value = dictionary["driverid"].string{ self.driverid  = value }
        if let value = dictionary["vehicleid"].string { self.vehicleid  = value }
        if let value = dictionary["startdate"].string  { self.startdate  = value }
        if let value = dictionary["deviceid"].string { self.deviceid  = value }
        if let value = dictionary["eventtype"].int { self.eventType = value}
    }
}
