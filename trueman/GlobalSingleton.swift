//
//  GlobalSingleton.swift
//  truman
//
//  Created by Mashesh Somineni on 9/29/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import Foundation
class GlobalSingleton {
    static let sharedInstance = GlobalSingleton()
    
    var selectedAppointmentId:String?
    private init() {
    
    
    }
}
