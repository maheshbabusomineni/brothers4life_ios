//
//  DateExtensions.swift
//  truman
//
//  Created by Mashesh Somineni on 9/26/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import Foundation
extension Date {
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: self)!
    }
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: self)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return tomorrow.month != month
    }
    func toString(dateFormat format: String ) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    static func convertDateFormatter(date: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
        let date = dateFormatter.date(from: date)
        
        dateFormatter.dateFormat = "MMM dd yyyy  EEEE HH:mm"///this is what you want to convert format
        let timeStamp = dateFormatter.string(from: date!)
        return timeStamp
    }
}
