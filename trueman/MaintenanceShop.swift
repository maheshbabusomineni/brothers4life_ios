//
//  MaintenanceShop.swift
//  truman
//
//  Created by Mashesh Somineni on 9/22/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import Foundation
import SwiftyJSON
import MapKit
import Contacts

class MaintenanceShop: NSObject, MKAnnotation{
    var coordinate: CLLocationCoordinate2D
    var title:String?
    var id:String?
    var name:String?
    var decription:String?
    var address:String?
    var city:String?
    var state:String?
    var country:String?
    var zipcode:String?
    var phone:String?
    var email:String?
    var latitude:Double?
    var longitude:Double?
    var logo:String?
    var open_time:Int?
    var close_time:Int?
    var major:Int?
    var minor:Int?
    var uuid:String?
    
    var timeSloptsArray = [Timeslot]()
    
    init(appointment:Appointment) {
        self.id = appointment.shopid
        self.name = appointment.name
        self.address = appointment.address
        self.phone = appointment.phone
        self.logo = appointment.receptionistphoto
        //self.starttime = appointment.starttime
        self.latitude = appointment.gpslat
        self.longitude = appointment.gpslong
        //self.endtime  = appointment.endtime
        self.email = appointment.email
        self.coordinate = CLLocationCoordinate2D(latitude: appointment.gpslat, longitude: appointment
            .gpslong)
        self.title = self.name
        self.uuid = appointment.uuid
        self.major = Int(appointment.major)
        self.minor = Int(appointment.minor)
        
    }
    init(dictionary:JSON) {
        if let value = dictionary["id"].string { self.id  = value }
        if let value = dictionary["name"].string { self.name  = value }
        if let value = dictionary["decription"].string { self.decription  = value }
        if let value = dictionary["address"].string { self.address  = value }
        if let value = dictionary["city"].string { self.city  = value }
        if let value = dictionary["state"].string { self.state  = value }
        if let value = dictionary["country"].string { self.country  = value }
        if let value = dictionary["zipcode"].string { self.zipcode  = value }
        if let value = dictionary["phone"].string { self.phone  = value }
        if let value = dictionary["email"].string { self.email  = value }
        if let value = dictionary["latitude"].double { self.latitude  = value }
        if let value = dictionary["longitude"].double { self.longitude  = value }
        if let value = dictionary["logo"].string { self.logo  = value }
        if let value = dictionary["open_time"].int { self.open_time  = value }
        if let value = dictionary["close_time"].int { self.close_time  = value }
        if let value = dictionary["major"].int { self.major  = value }
        if let value = dictionary["minor"].int { self.minor  = value }
        if let value = dictionary["uuid"].string { self.uuid  = value }
        
        let appointments = (Appointment.fetchByDate(date: Date().tomorrow.toString(dateFormat: Constants.dateFormat), context: Constants.managedObjectContext) as! [Appointment]).map{$0.starttime}
        
        for i in 0...((self.close_time)! - (self.open_time)!) {
            let starttime = (self.open_time)! + i
            let slotendtime = starttime + 1
            let startimeampm = (starttime >= 12) ? "PM" : "AM"
            let slotendtimeampm = ((starttime + 1) >= 12) ? "PM" : "AM"
            let slot = Timeslot(shopid: self.id!, slot: "\(starttime) \(startimeampm) - \(slotendtime) \(slotendtimeampm)")
            
            if appointments.contains(Int32(starttime)){
                slot.booked = true
            }
            self.timeSloptsArray.append(slot)
        }
        
        //if let value = dictionary["receptionistphoto"].string { self.receptionistphoto  = value }
        
        self.logo = "shop.png"
        self.coordinate = CLLocationCoordinate2D(latitude: self.latitude!, longitude: self.longitude!)
        self.title = self.name
        super.init()
       
    }
    var subtitle: String? {
        return address
    }
    
    func mapItem() -> MKMapItem {
        let addressDict = [CNPostalAddressStreetKey: subtitle!]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDict)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = self.title
        return mapItem
    }
    
    

}
