//
//  MapShopDetailsView.swift
//  truman
//
//  Created by Mashesh Somineni on 9/25/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import UIKit
import MapKit

class MapShopDetailsView: MKPinAnnotationView {
    @IBOutlet weak var shopImageView: UIImageView!
    @IBOutlet weak var shopNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var receptionistLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var timeslotsTableView: UITableView!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    

}
