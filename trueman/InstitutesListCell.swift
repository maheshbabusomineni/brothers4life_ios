//
//  InstitutesListCell.swift
//  truman
//
//  Created by Mashesh Somineni on 10/8/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import UIKit

class InstitutesListCell: UITableViewCell {
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    
    var item: Institute? = nil {
        didSet {
            //self.logoImageView.image = UIImage(named: self.item?.logo ?? "placeholder.png")
            self.logoImageView.image = UIImage(named:"placeholder.png")
            self.titleLabel.text = self.item?.name
            self.adressLabel.text = self.item?.address
            self.cityLabel.text = self.item?.city
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
