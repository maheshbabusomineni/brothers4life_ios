//
//  InstitutesListViewController.swift
//  truman
//
//  Created by Mashesh Somineni on 10/8/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import UIKit

class InstitutesListViewController: UITableViewController {
    let controller = InstitutesListController.sharedInstance
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        self.controller.delegate = self
        self.controller.processServiceRequest(request: RestAPIRouter.getShops())
    }
    
    @IBAction func refreshValueChanged(_ sender: Any) {
        self.controller.processServiceRequest(request: RestAPIRouter.getShops())
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.controller.objectsArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InstitutesListCell", for: indexPath) as! InstitutesListCell
        cell.item = self.controller.objectsArray[indexPath.row]
        return cell
        
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let destination = segue.destination as? InstituteDetailsViewController{
            guard let row = self.tableView.indexPathForSelectedRow?.row else {
                self.present(message: "Please select the institute to show the details")
                return
            }
            destination.detailsObject = self.controller.objectsArray[row]
        }
    }
    
    func hideRefreshControl(){
        if self.refreshControl != nil {
            self.refreshControl?.endRefreshing()
        }
    }
}
extension InstitutesListViewController:CommonDelegate{
    func showAlert(message: String) {
        func showAlert(message: String) {
            DispatchQueue.main.async {
                self.hideRefreshControl()
                self.present(title: Constants.appName ?? "n/a", message: "Please select time slot.", animated: true, dismiss: "Ok")
            }
        }
    }
    func listRefresh() {
        //Update UI on Main Thread
        DispatchQueue.main.async {
            self.hideRefreshControl()
            self.tableView.reloadData()
        }
    }
}
