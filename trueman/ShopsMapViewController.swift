//
//  ShopsMapViewController.swift
//  trueman
//
//  Created by Mashesh Somineni on 9/21/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ShopsMapViewController: UIViewController {
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var shopNameLabel: UILabel!
    @IBOutlet weak var shopAddressLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    var locationManager = CLLocationManager()

    var selectedAnnotation:MaintenanceShop? {
        didSet{
            self.shopNameLabel.text = selectedAnnotation?.title
            self.imageView.image = UIImage(named: (selectedAnnotation?.logo)!)
            self.shopAddressLabel.text = selectedAnnotation?.address
            self.phoneNumberLabel.text = selectedAnnotation?.phone
            self.tableView.reloadData()
        }
    }
    
    var shopsArray = [MaintenanceShop]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //Make a service call to get the data from server
        checkLocationAuthorizationStatus()
        self.mapView.delegate = self
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
        
        RestAPIController.startRequest(request: RestAPIRouter.getShops()){ serverResponse in
            guard serverResponse.error == nil else {
                self.present(message: (serverResponse.error?.localizedDescription) ?? "n/a" )
                return
            }
            
            //print("Response = \(serverResponse)")
            guard let responseArray = serverResponse.json?.array else { return }
            for dictionary in responseArray{
                let object = MaintenanceShop(dictionary: dictionary)
                self.shopsArray.append(object)
                
            }
            
            //Update UI on Main Thread
            DispatchQueue.main.async {
                self.mapView.addAnnotations(self.shopsArray)
                if !self.shopsArray.isEmpty{
                    self.selectedAnnotation = self.shopsArray.first
                    self.tableView.delegate = self
                    self.tableView.dataSource = self
                }
            }
        }
    }
    func centerMapOnLocation(location:CLLocation) {
        
        let regionRadius: CLLocationDistance = 11000
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        self.mapView.setRegion(coordinateRegion, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func confirmButtonClick(_ sender: Any) {
        guard let indexPath = self.tableView.indexPathForSelectedRow else {
            self.present(title: "Truman", message: "Please select time slot.", animated: true, dismiss: "Ok")
            return
        }
        
        print("Eventid = \(EventsController.sharedInstance.selectedEventId)")
        //Insert all records to core data
        DispatchQueue.global(qos: .userInitiated).async { // 1
            
            guard let shop = self.selectedAnnotation else {return}
            
            Appointment.insertObject(dictionary: ["shop":shop, "timeslot":(shop.timeSloptsArray[indexPath.row]),"eventid":EventsController.sharedInstance.selectedEventId,"eventtype":EventsController.sharedInstance.selectedEventType?.rawValue ?? 0] , context:Constants.managedObjectContext)
            
            self.startMonitoringItem(iBeacon(name: shop.name!, icon: 0, uuid:UUID(uuidString: shop.uuid!)!, majorValue: shop.major!, minorValue: shop.minor!))
                
            DispatchQueue.main.async { // 2
                TabBarController.changeTo(tab: .Appointments)
            }
        }
    }
    func startMonitoringItem(_ item: iBeacon) {
        let beaconRegion = item.asBeaconRegion()
        locationManager.startMonitoring(for: beaconRegion)
        locationManager.startRangingBeacons(in: beaconRegion)
    }
    
    func stopMonitoringItem(_ item: iBeacon) {
        let beaconRegion = item.asBeaconRegion()
        locationManager.stopMonitoring(for: beaconRegion)
        locationManager.stopRangingBeacons(in: beaconRegion)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            mapView.showsUserLocation = true
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
}
extension ShopsMapViewController: MKMapViewDelegate {
    // 1
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? MaintenanceShop {
            let identifier = "pin"
            var view: MKPinAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
                as? MKPinAnnotationView { // 2
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {
                // 3
                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                //view.canShowCallout = true
            }
                return view
        }
        return nil
    }
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        //let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
         //let location = view.annotation as! Artwork
         //location.mapItem().openInMaps(launchOptions: launchOptions)
    }
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        if let annotation = view.annotation as? MaintenanceShop {
            self.selectedAnnotation = annotation
        }
    }
    
}
extension ShopsMapViewController:CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //Center the map as location updates
        centerMapOnLocation(location: locations.last!)
    }
}

extension ShopsMapViewController:UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Available Slots"
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (selectedAnnotation?.timeSloptsArray.count)!
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TimeSlotCell", for: indexPath) as! TimeSlotCell
        cell.item = self.selectedAnnotation?.timeSloptsArray[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !(self.selectedAnnotation?.timeSloptsArray[indexPath.row].booked ?? false) {
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        }
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .none
    }
}

