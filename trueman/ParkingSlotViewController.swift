//
//  ParkingSlotViewController.swift
//  truman
//
//  Created by Mashesh Somineni on 9/22/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import EZLoadingActivity

class ParkingSlotViewController: UIViewController {
    
    @IBOutlet weak var pinLocationLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    lazy var geocoder = CLGeocoder()
    @IBOutlet weak var pinImageView: UIImageView!

    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        checkLocationAuthorizationStatus()
        self.mapView.delegate = self
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
        
        if let currentLocation = self.mapView.userLocation.location {
            centerMapOnLocation(location:currentLocation )
        }
        let mapDragRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.didDragMap(gestureRecognizer:)))
        mapDragRecognizer.delegate = self
        self.mapView.addGestureRecognizer(mapDragRecognizer)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            mapView.showsUserLocation = true
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    func centerMapOnLocation(location:CLLocation) {
        
        let regionRadius: CLLocationDistance = 10
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        self.mapView.setRegion(coordinateRegion, animated: true)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ParkingSlotViewController:CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //Center the map as location updates
        centerMapOnLocation(location: locations.last!)
    }
}
extension ParkingSlotViewController: MKMapViewDelegate {
    // 1
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? MaintenanceShop {
            let identifier = "pin"
            var view: MKPinAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
                as? MKPinAnnotationView { // 2
                view = dequeuedView
            } else {
                // 3
                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                
                let goButton = UIButton(type: .roundedRect)
                goButton.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
                goButton.setTitleColor(UIColor.red, for: .normal)
                goButton.setTitle("Start", for: .normal)
                view.rightCalloutAccessoryView = goButton
                
            }
            view.animatesDrop = true
            view.canShowCallout = true
            
            return view
        }
        return nil
    }
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        let location = view.annotation as! MaintenanceShop
        location.mapItem().openInMaps(launchOptions: launchOptions)
    }
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        if (view.annotation as? MaintenanceShop) != nil {
            //self.selectedAnnotation = annotation
        }
    }
    func mapView(_ mapView: MKMapView, rendererFor
        overlay: MKOverlay) -> MKOverlayRenderer {
        
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.red
        renderer.lineWidth = 5.0
        return renderer
    }
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        let center = self.mapView.centerCoordinate
        // Create Location
        let location = CLLocation(latitude: center.latitude, longitude: center.longitude)
        
        // Geocode Location
        geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
            // Process Response
            self.processResponse(withPlacemarks: placemarks, error: error)
        }
        
        // Update View
        //EZLoadingActivity.show("Please wait", disableUI: true)
    }
    
    private func processResponse(withPlacemarks placemarks: [CLPlacemark]?, error: Error?) {
        // Update View
        //EZLoadingActivity.hide()
  
        if let error = error {
            print("Unable to Reverse Geocode Location (\(error))")
            self.pinLocationLabel.text = "Unable to Find Address for Location"
            
        } else {
            if let placemarks = placemarks, let placemark = placemarks.first {
                self.pinLocationLabel.text = placemark.compactAddress
            } else {
                self.pinLocationLabel.text = "No Matching Addresses Found"
            }
        }
    }
}
extension ParkingSlotViewController:UIGestureRecognizerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    func didDragMap(gestureRecognizer: UIGestureRecognizer) {
        if (gestureRecognizer.state == UIGestureRecognizerState.began) {
            pinImageView.frame.size.height = pinImageView.frame.size.height * 0.8
        }
        if (gestureRecognizer.state == UIGestureRecognizerState.ended) {
            pinImageView.frame.size.height = pinImageView.frame.size.height * 1.2
        }
    }
}

