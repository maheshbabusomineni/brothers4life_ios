//
//  EventsCell.swift
//  truman
//
//  Created by Mashesh Somineni on 9/22/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import UIKit

class EventsListCell: UITableViewCell {
    @IBOutlet weak var recomendationLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var severityLabel: UILabel!

    var item: Event? = nil {
        didSet {
            self.titleLabel?.text = item?.event
            self.dateLabel?.text =  Date.convertDateFormatter(date: (item?.eventdate) ?? "n/a")
            self.recomendationLabel.text = item?.recommendation
            self.severityLabel.text = item?.severity
            
            /*if let item = item {
                imgIcon.image = Icons(rawValue: item.icon)?.image()
                lblName.text = item.name
                lblLocation.text = item.locationString().0
            } else {
                imgIcon.image = nil
                lblName.text = ""
                lblLocation.text = ""
            }*/
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
