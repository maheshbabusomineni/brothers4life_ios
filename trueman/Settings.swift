//
//  Settings.swift
//  trueman
//
//  Created by Mashesh Somineni on 9/13/17.
//  Copyright © 2017 Mashesh Somineni. All rights reserved.
//

import Foundation

class Settings : NSObject {
    
    open static func registerDefaults() {
        guard let settingsBundleURL = Bundle.main.url(forResource: "Settings", withExtension: "bundle") else { return }
        guard let settings = NSDictionary(contentsOf: settingsBundleURL.appendingPathComponent("Root.plist")) else { return }
        guard let preferences = settings["PreferenceSpecifiers"] as? [ [String:Any] ] else { return }
        
        var defaults: [String:Any] = [:]
        
        for preference in preferences {
            guard let key = preference["Key"] as? String else { continue }
            guard let defaultValue = preference["DefaultValue"] else { continue }
            
            defaults[key] = defaultValue
            
        }
        UserDefaults.standard.register(defaults: defaults)
        UserDefaults.standard.synchronize()
    }
    
    open static func setBool(key: String, value: Bool) {
        let standard = UserDefaults.standard
        standard.set(value, forKey: key)
    }
    open static func getBool(key: String) -> Bool? {
        let standard = UserDefaults.standard
        
        if let value = standard.value(forKey: key) as? Bool {
            return value
        }
        return nil
    }
    open static func getBool(key: String, defaultValue: Bool = false) -> Bool {
        if let value = self.getBool(key: key) {
            return value
        }
        return defaultValue
    }
    
    open static func setString(key: String, value: String) {
        let standard = UserDefaults.standard
        standard.set(value, forKey: key)
    }
    open static func getString(key: String) -> String? {
        let standard = UserDefaults.standard
        
        if let value = standard.value(forKey: key) as? String {
            return value
        }
        return nil
    }
    open static func getString(key: String, defaultValue: String) -> String {
        if let string = self.getString(key: key) {
            return string
        }
        return defaultValue
    }
    
    open static func setInteger(key: String, value: Int) {
        let standard = UserDefaults.standard
        standard.set(value, forKey: key)
    }
    open static func getInteger(key: String) -> Int? {
        let standard = UserDefaults.standard
        
        if let value = standard.value(forKey: key) as? String {
            if let intValue = Int(value) {
                return intValue
            }
        }
        return nil
    }
    open static func getInteger(key: String, defaultValue: Int = 0) -> Int {
        if let value = self.getInteger(key: key) {
            return value
        }
        return defaultValue
    }
    open static func remove(key:String){
        let standard = UserDefaults.standard
        standard.removeObject(forKey: key)
        standard.synchronize()
    }
}
